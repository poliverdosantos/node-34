const fs = require('fs');
const crearArchivo = async (base) => {
    try {
  let multiplicacion = '';
  let suma = '';
  let resta = '';
  let division = '';
  console.log('=================================');
  console.log(`    Operaciones Basicas valor: ${base}      `);
  console.log('=================================');

  for(let i = 1; i <= 100; i++){
    multiplicacion += `${base} x ${i} = ${base * i}\n`;
    suma += `${base} + ${i} = ${base + i}\n`;
    resta += `${base} - ${i} = ${base - i}\n`;
    division += `${base} / ${i} = ${base / i}\n`;
  }

  let operaciones ="Suma:" + '\n'+ suma + '\n' + "Resta:"+ '\n' +resta + '\n'+ "Multiplicacion:"+ '\n' + multiplicacion+ '\n'+"Division:"+ '\n' + division
  console.log(multiplicacion);
  console.log(suma);
  console.log(resta);
  console.log(division);

    fs.writeFileSync(`Tabla-Operaciones-Basicas.txt`, operaciones);
    return `Tabla-Operaciones-Basicas.txt`
    //console.log(`tabla-${base}.txt creado`);
  }
  catch (error) {
    throw error;
  }
};

//Exportamos el archivo
module.exports = {
  generarArchivo: crearArchivo
};
